package ru.btlab.btlredmine;

import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.bean.IssueStatus;
import com.taskadapter.redmineapi.bean.Project;
import com.taskadapter.redmineapi.bean.Role;
import hudson.Extension;
import hudson.ExtensionPoint;
import hudson.model.Describable;
import hudson.model.Descriptor;
import hudson.util.FormValidation;
import hudson.util.ListBoxModel;
import jenkins.model.Jenkins;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;

import javax.annotation.Nonnull;
import java.net.MalformedURLException;
import java.net.URL;

/***
 * Общий класс для хранения настроек плагина и взаимодействий в сервисами
 */
public class Config implements Describable<Config>, ExtensionPoint {

    @Override
    public PluginConfigDescriptor getDescriptor() {
        return (PluginConfigDescriptor) Jenkins.getInstance().getDescriptorOrDie(getClass());
    }

    @Extension
    public static class PluginConfigDescriptor extends Descriptor<Config> {

        private RedmineInfo redmineInfo;
        private RedmineService redmineService;

        public PluginConfigDescriptor() {
            super(Config.class);
            load();
            if (redmineInfo == null) {
                redmineInfo = new RedmineInfo();
            }
            if (redmineService == null) {
                redmineService = new RedmineService(redmineInfo);
            }
        }

        @Nonnull
        @Override
        public String getDisplayName() {
            return "BTLab redmine plugin config";
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject json) throws FormException {
            redmineInfo.setUrl(json.getString("redmineUrl"));
            redmineInfo.setApiKey(json.getString("redmineApiKey"));
            save();
            return true;
        }

        //++GETTERS
        public String getRedmineUrl() {
            return redmineInfo.getUrl();
        }

        public String getRedmineApiKey() {
            return redmineInfo.getApiKey();
        }
        //GETTERS--

        //++VALIDATION
        public FormValidation doCheckRedmineUrl(@QueryParameter String value) {
            try {
                new URL(value);
            } catch (MalformedURLException e) {
                return FormValidation.error("Неверный формат URL");
            }
            return FormValidation.ok();
        }

        public FormValidation doCheckRedmineApiKey(@QueryParameter String value) {
            if (StringUtils.isBlank(value)) {
                return FormValidation.error("Не указан api key");
            }
            return FormValidation.ok();
        }
        //VALIDATION--

        //++FILL DATA
        public ListBoxModel doFillRoleIdItems() {
            ListBoxModel rolesModel = new ListBoxModel();
            try {
                for (Role r : redmineService.getAllRoles()) {
                    rolesModel.add(new ListBoxModel.Option(r.getName(), String.valueOf(r.getId())));
                }
            } catch (Throwable e) {
                rolesModel.add(new ListBoxModel.Option(e.getMessage(), null, true));
            }

            return rolesModel;
        }

        public ListBoxModel doFillProjectKeyItems() {
            ListBoxModel projectsModel = new ListBoxModel();
            try {
                for (Project p : redmineService.getAllProjects()) {
                    projectsModel.add(new ListBoxModel.Option(p.getName(), p.getIdentifier()));
                }
            } catch (Throwable e) {
                projectsModel.add(new ListBoxModel.Option(e.getMessage(), null, true));
            }

            return projectsModel;
        }

        public ListBoxModel doFillIssuesStatusItems() {
            ListBoxModel statusesModel = new ListBoxModel();
            try {
                for (IssueStatus s : redmineService.getAllIssueStatuses()) {
                    statusesModel.add(new ListBoxModel.Option(s.getName(), String.valueOf(s.getId())));
                }
            } catch (Throwable e) {
                statusesModel.add(new ListBoxModel.Option(e.getMessage(), null, true));
            }
            return statusesModel;
        }
        //FILL DATA--

        //++Service methods
        public String getUserMailsByProjectKeyAndRoleId(String projectKey, String roleId) throws RedmineException {
            return redmineService.getUserMailsByProjectKeyAndRoleId(projectKey, Integer.parseInt(roleId));
        }

        public void changeRedmineTaskStatusFromToByProjectKey(String projectKey, String fromIssueStatus, String toIssueStatus)
                throws RedmineException {
            redmineService.changeRedmineTaskStatusFromToByProjectKey(projectKey,
                    Integer.valueOf(fromIssueStatus), Integer.valueOf(toIssueStatus));
        }
        //Service methods--
    }
}
