package ru.btlab.btlredmine;

import com.taskadapter.redmineapi.RedmineException;
import hudson.Extension;
import hudson.Launcher;
import hudson.model.*;
import hudson.tasks.BuildStep;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.Builder;
import hudson.util.ListBoxModel;
import jenkins.model.Jenkins;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.DataBoundConstructor;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collections;

/***
 * Билд шаг, для получения электронных адресов с сервера redmine
 */
public class GrabRedmineMailsBuildStep extends Builder implements BuildStep {

    private static final String REDMINE_MAIL_LIST = "REDMINE_MAIL_LIST";
    private final String projectKey;
    private final String roleId;

    @DataBoundConstructor
    public GrabRedmineMailsBuildStep(String projectKey, String roleId) {
        this.projectKey = projectKey;
        this.roleId = roleId;
    }

    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl) super.getDescriptor();
    }

    @Override
    public boolean perform(AbstractBuild<?, ?> build, Launcher launcher, BuildListener listener) throws InterruptedException, IOException {
        try {
            String existEmails = build.getEnvironment(listener).get(REDMINE_MAIL_LIST);
            String currentEmails = getDescriptor().getPluginConfig().getUserMailsByProjectKeyAndRoleId(projectKey, roleId);
            String emails;
            if (StringUtils.isBlank(existEmails)) {
                emails = currentEmails;
            } else {
                emails = existEmails + currentEmails;
            }
            StringParameterValue p = new StringParameterValue(REDMINE_MAIL_LIST, emails);

            ParametersAction a = build.getAction(ParametersAction.class);
            if (a != null) {
                build.replaceAction(a.createUpdated(Collections.singleton(p)));
            } else {
                build.addAction(new ParametersAction(p));
            }
        } catch (RedmineException e) {
            listener.getLogger().println("GrabRedmineMailsBuildStep cannot get emails, cause " + e.getMessage());
        }

        return true;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public String getRoleId() {
        return roleId;
    }

    @Extension
    public static class DescriptorImpl extends BuildStepDescriptor<Builder> {

        public DescriptorImpl() {
            super(GrabRedmineMailsBuildStep.class);
        }

        @Nonnull
        @Override
        public String getDisplayName() {
            return "Grad redmine mails";
        }

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> jobType) {
            return true;
        }

        /*package*/ Config.PluginConfigDescriptor getPluginConfig() {
            return (Config.PluginConfigDescriptor) Jenkins.getInstance().getDescriptorOrDie(Config.class);
        }

        public ListBoxModel doFillRoleIdItems() {
            return getPluginConfig().doFillRoleIdItems();
        }

        public ListBoxModel doFillProjectKeyItems() {
            return getPluginConfig().doFillProjectKeyItems();
        }
    }
}
