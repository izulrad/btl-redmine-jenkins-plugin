package ru.btlab.btlredmine;

import com.taskadapter.redmineapi.RedmineException;
import hudson.Extension;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.BuildListener;
import hudson.tasks.BuildStep;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Builder;
import hudson.util.ListBoxModel;
import jenkins.model.Jenkins;
import org.kohsuke.stapler.DataBoundConstructor;

import javax.annotation.Nonnull;
import java.io.IOException;

/***
 * Билд шаг, для смены статуса задачам redmine
 */
public class ChangeRedmineTaskStatusBuildStep extends Builder implements BuildStep {

    private final String projectKey;
    private final String fromIssueStatus;
    private final String toIssueStatus;

    @DataBoundConstructor
    public ChangeRedmineTaskStatusBuildStep(String projectKey, String fromIssueStatus, String toIssueStatus) {
        this.projectKey = projectKey;
        this.fromIssueStatus = fromIssueStatus;
        this.toIssueStatus = toIssueStatus;
    }

    @Override
    public BuildStepMonitor getRequiredMonitorService() {
        return BuildStepMonitor.NONE;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public String getFromIssueStatus() {
        return fromIssueStatus;
    }

    public String getToIssueStatus() {
        return toIssueStatus;
    }

    @Override
    public boolean perform(AbstractBuild<?, ?> build, Launcher launcher, BuildListener listener) throws InterruptedException, IOException {
        try {
            getDescriptor().getPluginConfig().changeRedmineTaskStatusFromToByProjectKey(projectKey, fromIssueStatus, toIssueStatus);
            listener.getLogger().println(
                    String.format("ChangeRedmineTaskStatusBuildStep change status for %s from %s to %s",
                            projectKey, fromIssueStatus, toIssueStatus));
        } catch (RedmineException e) {
            listener.getLogger().println("ChangeRedmineTaskStatusBuildStep do not change status, cause " + e.getMessage());
        }
        return true;
    }

    @Override
    public DescriptorImpl getDescriptor() {
        return (DescriptorImpl) super.getDescriptor();
    }

    @Extension
    public static class DescriptorImpl extends BuildStepDescriptor<Builder> {

        public DescriptorImpl() {
            super(ChangeRedmineTaskStatusBuildStep.class);
        }

        @Nonnull
        @Override
        public String getDisplayName() {
            return "Change redmine task status";
        }

        @Override
        public boolean isApplicable(Class<? extends AbstractProject> jobType) {
            return true;
        }

        /*package*/ Config.PluginConfigDescriptor getPluginConfig() {
            return (Config.PluginConfigDescriptor) Jenkins.getInstance().getDescriptorOrDie(Config.class);
        }

        public ListBoxModel doFillProjectKeyItems() {
            return getPluginConfig().doFillProjectKeyItems();
        }

        public ListBoxModel doFillFromIssueStatusItems() {
            return getPluginConfig().doFillIssuesStatusItems();
        }

        public ListBoxModel doFillToIssueStatusItems() {
            return getPluginConfig().doFillIssuesStatusItems();
        }
    }
}
