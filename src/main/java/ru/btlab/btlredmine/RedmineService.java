package ru.btlab.btlredmine;

import com.taskadapter.redmineapi.*;
import com.taskadapter.redmineapi.bean.*;
import org.apache.http.impl.client.HttpClientBuilder;

import java.util.List;

/**
 * Сервис для получения данных из redmine (rest-api)
 */
public class RedmineService {

    private final RedmineInfo redmineInfo;

    public RedmineService(RedmineInfo redmineInfo) {
        this.redmineInfo = redmineInfo;
    }

    /***
     * Конфигурирует и возвращает объект RedmineManager, согласно RedmineInfo
     *
     * @return скорфигурированный согласно RedmineInfo объект RedmineManager
     */
    private RedmineManager getRedmineManager() {
        return RedmineManagerFactory.createWithApiKey(
                redmineInfo.getUrl()
                , redmineInfo.getApiKey()
                , HttpClientBuilder.create().build()
        );
    }

    /***
     * Возвращает список всех проектов redmine
     *
     * @return список проектов redmine
     * @throws RedmineException исключение redmine-java-api
     */
    public List<Project> getAllProjects() throws RedmineException {
        return getRedmineManager().getProjectManager().getProjects();
    }

    /***
     * Возвращает список всех ролей redmine
     *
     * @return список ролей redmine
     * @throws RedmineException исключение redmine-java-api
     */
    public List<Role> getAllRoles() throws RedmineException {
        return getRedmineManager().getUserManager().getRoles();
    }

    /***
     * Возвращает список всех статусов задач redmine
     *
     * @return список статусов задач redmine
     * @throws RedmineException исключение redmine-java-api
     */
    public List<IssueStatus> getAllIssueStatuses() throws RedmineException {
        return getRedmineManager().getIssueManager().getStatuses();
    }

    /***
     * Конкатинирует в строку, через запятую, все адреса пользователей соответствующих роли в указанном проекте
     *
     * @param projectKey - имя проекта в redmine
     * @param roleId     - идентификатор роли в redmine
     * @return спсиок эелектронных адресов пользоватлей в проекте соответствующих роли
     * @throws RedmineException исключение redmine-java-api
     */
    public String getUserMailsByProjectKeyAndRoleId(String projectKey, int roleId) throws RedmineException {
        StringBuilder stringBuilder = new StringBuilder();
        RedmineManager rm = getRedmineManager();
        List<Membership> memberships = rm.getMembershipManager().getMemberships(projectKey);
        UserManager userManager = rm.getUserManager();

        for (Membership m : memberships) {
            boolean addUerMail = false;

            for (Role r : m.getRoles()) {
                if (r.getId() == roleId) {
                    addUerMail = true;
                    break;
                }
            }

            if (addUerMail) {
                String userMail = userManager.getUserById(m.getUser().getId()).getMail();
                stringBuilder
                        .append(userMail)
                        .append(",");
            }
        }

        return stringBuilder.toString();
    }

    /***
     * Изменяет статус всех задач в указаном проекте из fromIssueStatusId в toIssueStatusId
     *
     * @param projectKey        - имя проекта в redmine
     * @param fromIssueStatusId - идентификатор статуса задач, которые следует перевести вдругой статус
     * @param toIssueStatusId   - идентификатора статуса в который будет выставлен задачам в статусе fromIssueStatusId
     * @throws RedmineException исключение redmine-java-api
     */
    public void changeRedmineTaskStatusFromToByProjectKey(String projectKey, int fromIssueStatusId, int toIssueStatusId)
            throws RedmineException {
        IssueManager im = getRedmineManager().getIssueManager();
        List<Issue> issues = im.getIssues(projectKey, null);
        for (Issue i : issues) {
            if (i.getStatusId() == fromIssueStatusId) {
                i.setStatusId(toIssueStatusId);
                im.update(i);
            }
        }
    }
}
