Плагин для взаимодействия с внутренним redmine компании BTLab

Для работы плагина необходимо указать url redmine и api key в конфигурации Jenkins, секция "BTLab redmine plugin config"

_**Build step "Grad redmine mails"**_

Шаг сборки для сбора списка электронных адресов пользователей redmine, по проекту и роли. (Помещает собранный список в параметр $REDMINE_EMAIL_LIST).
Для работы необходимо разрешить передачу параметров внутри задачи, для этого необходимо запустить контейнер jenkins с параметрами
`-Dhudson.model.ParametersAction.safeParameters=REDMINE_MAIL_LIST`

_**Build step "ChangeRedmineTaskStatusNotifier"**_

Шаг сборки, изменяет статусы задач в указанном проекте.
Например со статуса "Выполнена" в статус "Готова к тестированию"


Запуск для разработки

`mvn package` - сборка плагина

`set MAVEN_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8000,suspend=n -Dhudson.model.ParametersAction.safeParameters=REDMINE_MAIL_LIST` - установка параметров контейнеру

`mvn hpi:run` - запуск локлаьного дженкинс для тестирования